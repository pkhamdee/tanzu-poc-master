package com.poc.mbket.controller;

import com.poc.mbket.dto.AppRequest;
import com.poc.mbket.dto.AppResponse;
import com.poc.mbket.dto.AppUpdateRequest;
import com.poc.mbket.entity.AppEntity;
import com.poc.mbket.exception.NotFoundException;
import com.poc.mbket.mapper.AppMapper;
import com.poc.mbket.repository.AppRepository;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Setter(onMethod_ = @Autowired)
@Slf4j
@Validated
public class AppController {
    private AppMapper appMapper;
    private AppRepository appRepository;

    @PostMapping(value = "/api/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestHeader("request_id") String requestId,
                                       @RequestBody @Valid AppRequest request) {
        log.info("Creating entity");
        AppEntity entity = appMapper.toEntity(request);
        appRepository.save(entity);
        return ResponseEntity.ok().build();
    }

    @Transactional
    @PutMapping(value = "/api/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@RequestHeader("request_id") String requestId, @PathVariable("id") Long id, @RequestBody @Valid AppUpdateRequest request) {
        log.info("Retrieving entity");
        AppEntity appEntity = appRepository.findById(id).orElseThrow(() -> new NotFoundException(id + "not found"));
        appEntity.setText(request.getText());
        appEntity.setFlag(request.getFlag());
        appRepository.save(appEntity);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/api/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AppResponse> retrieve(@RequestHeader("request_id") String requestId, @PathVariable("id") Long id) {
        log.info("Retrieving entity");
        AppEntity appEntity = appRepository.findById(id).orElseThrow(() -> new NotFoundException(id + " not found"));

        return ResponseEntity.ok(appMapper.fromEntity(appEntity));
    }
}
