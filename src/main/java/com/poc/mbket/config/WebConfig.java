package com.poc.mbket.config;

import com.poc.mbket.controller.interceptor.LogRequestIdInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logRequestIdInterceptor()).addPathPatterns("/**");
    }

    @Bean
    public LogRequestIdInterceptor logRequestIdInterceptor() {
        return new LogRequestIdInterceptor();
    }
}
